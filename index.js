const express = require('express'); // import express js library
const app = express(); //create express js instance 
const path = require('path');
const auth = require('./middleware/auth.js');

// define a route to download a file 
app.get('/download/audio/:file(*)',auth.validateToken, (req, res) => {
  var token = req.header.token;
  var file = req.params.file;
  var fileLocation = path.join("../audio/" + file);
  console.log(fileLocation);
  res.download(fileLocation, file);
});

app.get('/download/image/:file(*)', (req, res) => {
  var file = req.params.file;
  var fileLocation = path.join("../image/" + file);
  console.log(fileLocation);
  res.download(fileLocation, file);
});

app.use(function (err, req, res, next) {
  console.error(err.stack)
  res.status(404).send('')
})

app.listen(8001, () => {
  console.log(`application is running at: http://localhost:8001`);
});