const axios = require('axios');

module.exports = {
    validateToken: function(req, res, next) {
        var token = req.headers.authorization;
        var name = req.params.file;

        axios.get('http://127.0.0.1:2405/api/audio/check',{
            params:{
                token: token,
                audioName : name
            }
        })
        .then(res => {
          next();
        })
        .catch(err => {
          next(err);
        });
    }
}  